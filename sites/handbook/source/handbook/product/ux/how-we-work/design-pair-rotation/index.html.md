---
layout: handbook-page-toc
title: "Product Design Pairs"
description: "Product designer pairs rotation schedule"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

This is the rotation schedule for FY24-Q1 and Q2 (February 2023 - July 2023). Learn more about [Pair Designing](/handbook/product/ux/how-we-work/#pair-designing).

[//]: # TIP: Create the schedule in a temporary spreadsheet, and then copy/paste the rows into an online markdown generator (https://www.google.com/search?q=copy-table-in-excel-and-paste-as-a-markdown-table)

| Designer               | Design Pair            |
|------------------------|------------------------|
| Katie Macoy	         | Libor Vanc             |
| Veethika Mishra	 | Julia Miocene          |
| Gina Doyle	         | Emily Sybrant |
| Sunjung Park	         | Becka Lippert          |
| Pedro Moreira da Silva | Matt Nearents |
| Kevin Comoli	         | Camellia Yang          |
| Dan Mizzi-Harris	 | Jeremy Elder           |
| Sascha Eggenberger	 | Annabel Gray           |
| Mike Nichols	         | Amelia Bauerly         |
| Nick Leonard	         | Nick Brandt            |
| Austin Regnery	 | Michael Fangman        |
| Alex Fracazo	         | Michael Le             |
| Emily Bauman          | Ali Ndlovu             |

### Temporary pairing

No temporary pairings at this time.
