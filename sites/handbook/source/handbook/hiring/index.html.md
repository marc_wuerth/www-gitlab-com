---
layout: handbook-page-toc
title: "Hiring & Talent Acquisition Handbook"
description: "Landing page for many of the handbook pages the talent acquisition team at GitLab uses."
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

{::options parse_block_html="true" /}


## Introduction 
At the core of our team’s vision, mission, and strategy is our ability to impact GitLab’s overarching [mission](https://about.gitlab.com/company/mission/): to make it so that **everyone can contribute**. When **everyone can contribute**, users become contributors and we greatly increase the rate of innovation. 

As a Talent Acquisition team, we have an outsized impact on GitLab’s ability to make this mission a reality, by connecting top talent to profound careers from wherever they are in a truly distributed, remote workforce. 

### Talent Acquisition Vision Statement
To create globally inclusive access to opportunities so that **everyone can contribute**.

### Talent Acquisition Mission Statement
It is the Talent Acquisition Team’s mission to predictably build distributed, representative teams that enable our vision of creating globally inclusive access to opportunities so that **everyone can contribute**. 

### Our Guiding Principles
As we set out over the next decade to achieve this vision, we will continue to rely on core guiding principles to define how we build toward the future. 
  1. **Experience**: Stakeholder experience is central to all that we do. We are not purely a service provider but a partner and advisor in creating a best-in-class experience while building GitLab as a company. The Talent Acquisition team looks after building an experience for our customers, stakeholders, and partners that stands true to our [CREDIT](https://about.gitlab.com/handbook/values/) values and provides a level of care we are proud of - no matter the outcome. 
  1. **Inclusivity**: Applying best practices in the craft of Talent Acquisition has an outsized impact on our ability to build a representative workforce here at GitLab. By embedding inclusivity as a guiding principle, we design fair and equitable processes into the fabric of all that we do, rather than retroactively adding parameters to solve systemic challenges in the societies we exist in   
  1. **Predictability**: Our ability to have the right people, in the right jobs, at the right time is imperative to our ability to execute our commitments and plans as an organization. In order to achieve that, our team must design with an eye for accuracy and forecastability in any program, process or experience we enable. 

### Our Objectives and Roadmap
We strive to be as transparent as possible, but these sections are only available for our GitLab team members.
[FY24 TA Strategy](https://docs.google.com/document/d/1Q9ui_BZLRPBAYpRtdtpHVSvGzW3g7JtVYkGj_KqwZBA/edit#heading=h.riu5wc8sjmum)


## Talent Acquisition Pages and Processes

### Team Process Pages

- [TA Key Performance Indicators](https://internal-handbook.gitlab.io/handbook/people-group/talent-acquisition/key-performance-indicators/) Note: this page is currently in our internal handbook as they are a work in progress.
- [Meeting Cadence](/handbook/hiring/meetings/)
- [Talent Acquisition Alignment](/handbook/hiring/recruiting-alignment/)
- [Diversity, Inclusion & Belonging Talent Acquisition Initiatives](/company/culture/inclusion/talent-acquisition-initiatives/)
- [Triad Process](/handbook/hiring/talent-acquisition-framework/triadprocess/)

<details>
<summary markdown="span">Shared Definitions</summary>

* **Job:** A job refers to the job title (ex: Customer Support Specialist). This will also be what appears on external job boards. In the case there are multiple positions open that are the same, and we only want to list once, we can have multiple 'openings' (see next section) opened within one 'Job'. Each job will have a unique identifier called a Requisition ID (example- 1001).

* **Opening:** A job can have multiple openings attached to it (ex: you are hiring 3 Customer Support Specialists. You would then have 1 ‘Job’ and 3 ‘openings’ against that job). A job can have multiple openings against it, but an opening can not be associated with multiple jobs. Each opening will have a unique identifier called an Opening ID (example- 1001-1, 1001-2, 1001-3).

* **GHPiD:** GHP ID is the link between Adaptive (what we use to track our operating plan) and Greenhouse (our ATS). A GHP ID has a one to one relationship with an Opening ID. It is the key interlock between our hiring plans and our Talent Acquisition activity. This is a custom field in Greenhouse.

</details>


### Candidate Handbook Pages
Please find pages for potential and active applicants below.

- [Candidate Handbook Page](/handbook/hiring/candidate/faq/)
- [Talent Acquisition Privacy Policy](/handbook/hiring/candidate/faq/recruitment-privacy-policy/)


### Interviewer Processes

- [Interviewer Prep Requirements](/handbook/hiring/interviewing/)
- [Conducting a GitLab Interview](/handbook/hiring/conducting-a-gitlab-interview/)
- [Greenhouse for Interviewers](/handbook/hiring/greenhouse/#for-all-interviewers/)


### Hiring Manager Processes

- [Hiring Manager Processes](/handbook/hiring/talent-acquisition-framework/hiring-manager/)
- [Conducting a GitLab Interview](/handbook/hiring/conducting-a-gitlab-interview/)
- [Greenhouse for Hiring Managers](/handbook/hiring/greenhouse/#for-hiring-managers)

### Candidate Experience Specialist Processes

<details>
<summary markdown="span">Greenhouse integrations you'll need</summary>
  * [Prelude](/handbook/hiring/prelude/): To gain CES-level access to Prelude, ask your manager to message the Support team at Prelude.<br>
  * [Guide](https://support.greenhouse.io/hc/en-us/articles/360052205072-Guide-integration): Check with your manager if you do not have higher level access to navigate inside of Prelude.<br>
  * [DocuSign](https://support.greenhouse.io/hc/en-us/articles/205633569-DocuSign-integration)

</details>

- [Candidate Experience Specialist Responsibilities](/handbook/hiring/talent-acquisition-framework/coordinator/)
- [Prelude](/handbook/hiring/prelude)
- [How to Complete a Contract - CES Process](/handbook/hiring/talent-acquisition-framework/ces-contract-processes/)


### Hiring Pages

- [Greenhouse](/handbook/hiring/greenhouse/)
- [Overview of Job Families](/handbook/hiring/job-families)
- [People Technology & Insights](/handbook/hiring/talent-acquisition-framework/talent-acquisition-operations-insights/)
- [Talent Acquisition Process Framework](/handbook/hiring/talent-acquisition-framework/)
- [Referral Operations](/handbook/hiring/referral-operations/)
- [Referral Process](/handbook/hiring/referral-process/)
- [Resource Guide](/handbook/hiring/guide/)
- [Sourcing](/handbook/hiring/sourcing/)



## Additional Resources

- [Background checks](/handbook/people-policies/#background-checks)
- [Benefits](/handbook/total-rewards/benefits/)
- [Compensation](/handbook/total-rewards/compensation/)
- [Contracts](https://internal-handbook.gitlab.io/handbook/people-group/people-operations/people-connect/employment_contracts/)
- [GitLab talent ambassador](/handbook/hiring/gitlab-ambassadors/)
- [Onboarding](/handbook/people-group/general-onboarding)
- [Stock options](/handbook/stock-options)
- [Visas](/handbook/people-group/visas/)
