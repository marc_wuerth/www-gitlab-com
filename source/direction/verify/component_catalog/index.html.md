---
layout: markdown_page
title: "Category Direction - Component Catalog"
description: "Reusable pipeline configuration and definitions" 
canonical_path: "/direction/verify/component_catalog/"
---

- TOC
{:toc}

## Our vision
Foster a collaborative community of developers who can easily share, build, and maintain high-quality CI/CD configurations. By providing a platform that embraces the open-source process, we aim to empower developers to focus on true innovation and unlock the full potential of the open-source ecosystem. Through our commitment to inclusivity, transparency, and accessibility, we strive to create a culture of continuous learning and improvement, where every community member can contribute.

## Our Mission
Provide the best-in-class experience for building and maintaining CI/CD pipelines.

## Glossary
This section defines the terminology throughout this project. With these terms we are only identifying abstract concepts and are subject to changes as we refine the design by discovering new insights.

* **Component** The reusable unit of pipeline configuration.
* **Project** The GitLab project attached to a repository. A project can contain multiple components.
* **Catalog** The collection of projects that are set to contain components, which will be broken down into two flavors:
  * **Private catalog** Scoped per namespace
  * **Public catalog** Single source of truth
* **Version** The release name of a tag in the project, which allows components to be pinned to a specific revision.

### Components catalog
The component catalog will be a collection of components which:

* Is the SSOT for users to browse and search for the pipeline components they desire.
* Contains shareable and reusable packages of YAML configurations (components) that could be attached to any CI/CD configuration.
* Has an easy way to document usage of each component.
* Allows users to contribute to the catalog by developing and publishing their components.

### Pipeline component
A pipeline component is a reusable single-purpose building block that abstracts away a single pipeline configuration unit. Components are used to compose a part or entire pipeline configuration. It can optionally take input parameters and set output data to be adaptable and reusable in different pipeline contexts while encapsulating and isolating implementation details.

Components allow a pipeline to be assembled by using abstractions instead of having all the details defined in one place. Therefore, when using a component in a pipeline, a user shouldn't need to know the implementation details of the component and should only rely on the provided interface.

A pipeline component defines its type which indicates in which context of the pipeline configuration the component can be used. For example, a component of type X can only be used according to the type X use-case.

For best experience with any systems made of components, it's fundamental that components are:

* Single purpose: A component must focus on a single goal and the scope must be as small as possible.
* Isolated: When a component is used in a pipeline, its implementation details should not leak outside the component itself into the main pipeline.
* Reusable: A component is designed to be used in different pipelines. Depending on the assumptions it's built on, a component can be more or less generic. Generic components are more reusable but may require more customization.
* Versioned: When using a component we must specify the version we are interested in. The version identifies the exact interface and behavior of the component.
* Resolvable: When a component depends on another component, this dependency must be explicit and trackable.

Some of those best practices will be enforced in the product while others would need to be enforced by the user.


### Roadmap


| Track |  Details | Milestone | status|
| ------ | ------ | ------ | -----|
| 1 |  Release `spec:inputs` and interpolation as `Beta`.       |  15.11  | Done
| 2 |  Release experimental CI/CD components   |  16.0 | Done
| 3 | Release experimental CI/CD components catalog | 16.1 | In progress
| 4 |  Make inputs/interpolation a GA feature. | 16.2 | In progress
| 5  |  Aim to make CI/CD catalog/components `Beta`. Improved release workflow, improved Catalog UX, and stable directory structure. | 16.3 | In progress
| 6 | [Introduce GitLab steps](https://gitlab.com/gitlab-org/gitlab/-/issues/357669) | ability to define a step 16.5| In planning phase
|7| [Visability to Components usage](https://gitlab.com/gitlab-org/gitlab/-/issues/393326) | Provide an analytic dashboard to component usage| tbd| design

* [Introduce CI/CD component](https://gitlab.com/groups/gitlab-org/-/epics/9409) - Implement an MVC of the CI/CD component functionality, dogfood it by converting existing templates into components, and release it as beta.
* [CI/CD Components Catalog](https://gitlab.com/groups/gitlab-org/-/epics/9410) - Implement an MVC of a CI/CD component which will be the user interface layer and the scaffolding for components in an organization.
* [Introduce GitLab steps](https://gitlab.com/gitlab-org/gitlab/-/issues/357669) - A step would be the script that the runner will run. We would like to allow steps to support running GitHub actions
* To address the misperception surrounding [AI DevOps](https://gitlab.com/gitlab-org/gitlab/-/issues/408839) GitLab proposes including it as a CI component in the CI/CD Components Catalog. This move aims to improve visibility and showcase the robustness of AI DevOps within the GitLab DevSecOps Platform. By positioning it as a starting point for DevOps transformation, organizations can discover and leverage the comprehensive and opinionated nature of Auto DevOps. By incorporating Auto DevOps as a CI component, GitLab aims to engage users and encourage them to explore its powerful features for their CI/CD workflows.
* [Intellegent search powered by generative AI](https://gitlab.com/gitlab-org/gitlab/-/issues/412663) - Unlike traditional keyword-based searches, Intelligent Search understands the intent behind user queries, enabling it to provide more accurate and relevant results. In addition, we can leverage AI to prompt more information about specific inputs required by the user, which will result in configured and fine-tuned components with the relevant inputs. This could enhance the user experience and streamline the process of selecting, configuring, and using components from the catalog. 

## What's Next
* The work for the [CI/CD Components Catalog](https://gitlab.com/groups/gitlab-org/-/epics/9410) epic is underway. In this phase, we would like to release the CI/CD catalog index page

* Continue the discussion with internal team as they dogfood some of our popular CI/CD [template to component](https://gitlab.com/gitlab-org/gitlab/-/issues/390656)


## Dogfooding
The best way to understand how GitLab works is to use it for as much of your job as possible, this is why we practice [dogfooding](https://about.gitlab.com/handbook/values/#dogfooding) We have recently begun collaborating with our internal teams in GitLab which express their desire to dogfood some of our features: 
* Productivity team - this effort is tracked through this [collaboration issue](https://gitlab.com/gitlab-org/quality/engineering-productivity/team/-/issues/141), and the team is currently identifying commonly used CI/CD templates and attempting to convert them into components. Our goal is to understand our customers' challenges when converting their templates into components. 
* Incubation team - this effort is tracked through this [collaboration issue](https://gitlab.com/gitlab-org/gitlab/-/issues/399480)
* Secure - We've converted some of our existing templates such as SASTS, Secret detection and more, this effort is tracked through this [collabortion issue](https://gitlab.com/gitlab-org/gitlab/-/issues/390656)

## Competitive Landscape
Notable competitors in this space are:

- GitHub actions with their [actions marketplace](https://github.com/marketplace?category=&query=&type=actions&verification=)
- [Circle CI orbs](https://circleci.com/developer/orbs)
- CodeFresh also provide their users with a [CI step library](https://codefresh.io/steps/)

Watch this walkthrough video of the different contribution frameworks available by these competitors:

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/7WSWGDtMD7A" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

## FAQ

**Q1 - Why are you not using templates?**

* GitLab CI/CD Templates have not been designed with reusability in mind. Within a template a user can define global keywords such as `variables:` that affects the entire pipeline leading to inheritance issues. Users should be able to convert their existing templates to CI/CD components.

**Q2 - Why aren't you using variables?**

* Up until now, users have used environment variables to provide information to templates or downstream pipelines. However, this can be problematic because global variables can potentially affect all pipelines (and templates). The more variables you use, the more likely it is for them to have an impact on your entire pipeline. It's important to note that variable interpolation will still be supported, and users could still use variables within a component but the way to pass parameters to a component will be through a declarative input schema.
