---
'16.0':
- Auto DevOps no longer provisions a database by default
- Azure Storage Driver defaults to the correct root prefix
- Bundled Grafana Helm Chart
- CAS OmniAuth provider is removed
- CiCdSettingsUpdate mutation renamed to ProjectCiCdSettingsUpdate
- Conan project-level search returns only project-specific results”
- Configuring Redis config file paths using environment variables is no longer supported
- Container Registry pull-through cache is removed
- Container Scanning variables that reference Docker removed
- Default value of ttl_days now 30 days
- Dependency Scanning ends support for Java 13, 14, 15, and 16
- Developer role providing the ability to import projects to a group
- Embedding Grafana panels in Markdown is removed
- Enforced validation of CI/CD parameter character lengths
- GitLab administrators must have permission to modify protected branches or tags
- GitLab.com importer
- 'GraphQL API: Runner status no longer returns PAUSED and ACTIVE values'
- Jira DVCS connector for Jira Cloud and Jira 8.13 and earlier
- Legacy Gitaly configuration method
- Legacy Gitaly configuration methods with variables
- Legacy Praefect configuration method
- Legacy routes removed
- License-Check and the Policies tab on the License Compliance page
- Limit CI_JOB_TOKEN scope is disabled
- Managed Licenses API
- Maximum number of active pipelines per project limit (ci_active_pipelines)
- Monitoring performance metrics through Prometheus is removed
- Non-expiring access tokens no longer supported
- Non-standard default Redis ports are no longer supported
- PipelineSecurityReportFinding name GraphQL field
- PostgreSQL 12 compatibility
- Praefect custom metrics endpoint configuration
- Project REST API field operations_access_level removed
- Rake task for importing bare repositories
- Redis 5 compatibility
- Removal of job_age parameter in POST /jobs/request Runner endpoint
- Remove legacy configuration fields in GitLab Runner Helm Chart
- Remove the deprecated environment_tier parameter from the DORA API
- Removed external field from GraphQL ReleaseAssetLink type
- Removed external field from Releases and Release link APIs
- Secure JWT token setting is removed
- Secure scanning _DISABLED variables now require the value "true"
- Security report schemas version 14.x.x
- Self-monitoring project is removed
- Starboard directive in the config for the GitLab agent for Kubernetes removed
- Stop publishing GitLab Runner images based on Windows Server 2004 and 20H2
- The Phabricator task importer
- The Security Code Scan-based GitLab SAST analyzer is now removed
- The stable Terraform CI/CD template has been replaced with the latest template
- Two DAST API variables have been removed
- Use of id field in vulnerabilityFindingDismiss mutation
- Vulnerability confidence field
- CI_BUILD_* predefined variables removed
- CI_PRE_CLONE_SCRIPT variable on GitLab SaaS Runners has been removed
- POST /projects/:id/merge_requests/:merge_request_iid/approvals removed
- POST ci/lint API endpoint removed
- docker-ssh and docker-ssh+machine executors are removed
- vulnerabilityFindingDismiss GraphQL mutation
'15.11':
- Exporting and importing projects in JSON format not supported
- openSUSE Leap 15.3 packages
'15.9':
- Live Preview no longer available in the Web IDE
- omniauth-authentiq gem no longer available
- omniauth-shibboleth gem no longer available
'15.8':
- CiliumNetworkPolicy within the auto deploy Helm chart is removed
- Exporting and importing groups in JSON format not supported
- artifacts:public CI/CD keyword refactored
'15.7':
- File Type variable expansion in .gitlab-ci.yml
- Flowdock integration
'15.6':
- NFS as Git repository storage is no longer supported
'15.4':
- SAST analyzer consolidation and CI/CD template changes
'15.3':
- Support for Debian 9
- Vulnerability Report sort by State
- Vulnerability Report sort by Tool
'15.2':
- Support for older browsers
'15.0':
- 'API: stale status returned instead of offline or not_connected'
- Audit events for repository push events
- Background upload for object storage
- Container Network and Host Security
- Container registry authentication with htpasswd
- Custom geo:db:* Rake tasks are no longer available
- DS_DEFAULT_ANALYZERS environment variable
- Dependency Scanning default Java version changed to 17
- ELK stack logging
- Elasticsearch 6.8.x in GitLab 15.0
- End of support for Python 3.6 in Dependency Scanning
- External status check API breaking changes
- GitLab Serverless
- Gitaly nodes in virtual storage
- GraphQL permissions change for Package settings
- Jaeger integration
- Known host required for GitLab Runner SSH executor
- Legacy Geo Admin UI routes
- Legacy approval status names in License Compliance API
- Move Gitaly Cluster Praefect database_host_no_proxy and database_port_no_proxy configs
- Move custom_hooks_dir setting from GitLab Shell to Gitaly
- OAuth implicit grant
- OAuth tokens without an expiration
- Optional enforcement of SSH expiration
- Optional enforcement of personal access token expiration
- Out-of-the-box SAST (SpotBugs) support for Java 8
- Pipelines field from the version field
- Pseudonymizer
- Request profiling
- Required pipeline configurations in Premium tier
- Retire-JS Dependency Scanning tool
- Runner status not_connected API value
- SAST support for .NET 2.1
- SUSE Linux Enterprise Server 12 SP2
- Secret Detection configuration variables
- Self-managed certificate-based integration with Kubernetes feature flagged
- Sidekiq configuration for metrics and health checks
- Static Site Editor
- Support for gitaly['internal_socket_dir']
- Support for legacy format of config/database.yml
- Test coverage project CI/CD setting
- The promote-db command is no longer available from gitlab-ctl
- Update to the Container Registry group-level API
- Versions from PackageType
- Vulnerability Check
- Managed-Cluster-Applications.gitlab-ci.yml
- artifacts:reports:cobertura keyword
- defaultMergeCommitMessageWithDescription GraphQL API field
- dependency_proxy_for_private_groups feature flag
- omniauth-kerberos gem
- promote-to-primary-node command from gitlab-ctl
- push_rules_supersede_code_owners feature flag
- type and types keyword from CI/CD configuration
- bundler-audit Dependency Scanning tool
'14.10':
- Permissions change for downloading Composer dependencies
'14.9':
- Integrated error tracking disabled by default
'14.6':
- Limit the number of triggered pipeline to 25K in free tier
- Release CLI distributed as a generic package
'14.3':
- Introduced limit of 50 tags for jobs
- List project pipelines API endpoint removes name support in 14.3
- Use of legacy storage setting
'14.2':
- Max job log file size of 100 MB
'14.1':
- Remove support for prometheus.listen_address and prometheus.enable
- Remove support for older browsers
'14.0':
- Auto Deploy CI template v1
- Breaking changes to Terraform CI template
- Code Quality RuboCop support changed
- Container Scanning Engine Clair
- DAST default template stages
- DAST environment variable renaming and removal
- Default Browser Performance testing job renamed in GitLab 14.0
- Default DAST spider begins crawling at target URL
- Default branch name for new repositories now main
- Dependency Scanning
- Deprecated GraphQL fields
- DevOps Adoption API Segments
- Disk source configuration for GitLab Pages
- Experimental prefix in Sidekiq queue selector options
- External Pipeline Validation Service Code Changes
- Geo Foreign Data Wrapper settings
- GitLab OAuth implicit grant
- GitLab Runner helper image in GitLab.com Container Registry
- GitLab Runner installation to ignore the skel directory
- Gitaly Cluster SQL primary elector
- Global SAST_ANALYZER_IMAGE_TAG in SAST CI template
- Hardcoded master in CI/CD templates
- Helm v2 support
- Legacy DAST domain validation
- Legacy feature flags
- Legacy fields from DAST report
- Legacy storage
- License Compliance
- Limit projects returned in GET /groups/:id/
- Make pwsh the default shell for newly-registered Windows Runners
- Migrate from SAST_DEFAULT_ANALYZERS to SAST_EXCLUDED_ANALYZERS
- Off peak time mode configuration for Docker Machine autoscaling
- OpenSUSE Leap 15.1
- PostgreSQL 11 support
- Redundant timestamp field from DORA metrics API payload
- Release description in the Tags API
- Ruby version changed in Ruby.gitlab-ci.yml
- SAST analyzer SAST_GOSEC_CONFIG variable
- Service Templates
- Success and failure for finished build metric conversion
- Terraform template version
- Ubuntu 16.04 support
- Ubuntu 19.10 (Eoan Ermine) package
- Unicorn in GitLab self-managed
- WIP merge requests renamed ‘draft merge requests’
- Web Application Firewall (WAF)
- Windows Server 1903 image support
- Windows Server 1909 image support
- "/usr/lib/gitlab-runner symlink from package"
- "?w=1 URL parameter to ignore whitespace changes"
- CI_PROJECT_CONFIG_PATH variable
- FF_RESET_HELPER_IMAGE_ENTRYPOINT feature flag
- FF_SHELL_EXECUTOR_USE_LEGACY_PROCESS_KILL feature flag
- FF_USE_GO_CLOUD_WITH_CACHE_ARCHIVER feature flag
- secret_detection_default_branch job
- trace parameter in jobs API
